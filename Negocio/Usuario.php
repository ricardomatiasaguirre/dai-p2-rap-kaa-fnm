<?php
namespace Negocio;

include 'bd.php';
include_once  './Entidades/Usuario.php';

use Entidades as ent;

class Usuario
{
    private $bd;
    function __construct()
    {
        $this->bd = new BD();
    }

    public function obtenerUsuario($usuario)
    {
        $username = $usuario->getUsername();
        $clave = $usuario->getClave();
        $result = $this->bd->select("select username, clave from usuario where username='$username' and clave='$clave'");
        $usuarios = [];
        foreach ($result as $usuario) {
            array_push($usuarios, new ent\Usuario($usuario["username"], $usuario["clave"]));
        }
        if (sizeof($usuarios) > 0)
            return  $usuarios[0];
        else
            return NULL;
    }

    public function obtenerListaUsuarios()
    {
        $result = $this->bd->query("select username from usuario");
        $usuarios = [];
        foreach ($result as $usuario) {
            array_push($usuarios, new ent\Usuario($usuario["username"]));
        }
        return $usuarios;
    }

    public function modificarUsuario($usuario)
    {
        $username = $usuario->getUsername();
        $rut = $usuario->getRut();
        $nombre = $usuario->getNombre();
        $apellidos = $usuario->getApellidos();
        $correo = $usuario->getCorreo();
        $edad = $usuario->getEdad();
        $region = $usuario->getRegion();
        $provincia = $usuario->getProvincia();
        //$cod_articulo = $usuario->get();
        $comuna = $usuario->getComuna();
        //$fechaCreacion = $usuario->getReg_fecha();
        $fecha_mod = $usuario->getMod_fecha();
        //$admin = $usuario->getAdmin();
        $SQL = "insert into Usuario (rut,nombre,apellido,correo,edad,region,provincia,comuna,mod_fecha) values ('$rut','$nombre','$apellidos','$correo','$edad','$region','$provincia','$comuna','$fecha_mod') WHERE username='$username'";

        return $this->bd->query($SQL);
    }
}
