<?php
namespace Entidades;


ini_set("display_errors",1);
ini_set("display_startup_errors",1);

/*
| id           | int(6) unsigned | NO   | PRI | NULL                | auto_increment                |
| username      | varchar(30)     | YES  |     | NULL                |                               |
| clave        | varchar(30)     | NO   |     | NULL                |                               |
| rut          | varchar(30)     | NO   |     | NULL                |                               |
| nombre       | varchar(30)     | YES  |     | NULL                |                               |
| apellido     | varchar(30)     | YES  |     | NULL                |                               |
| correo       | varchar(50)     | YES  |     | NULL                |                               |
| edad         | int(9)          | YES  |     | NULL                |                               |
| region       | varchar(30)     | YES  |     | NULL                |                               |
| provincia    | varchar(30)     | YES  |     | NULL                |                               |
| comuna       | varchar(30)     | YES  |     | NULL                |                               |
| cod_articulo | int(6)          | YES  | MUL | NULL                |                               |
| reg_fecha    | timestamp       | NO   |     | current_timestamp() | on update current_timestamp() |
| mod_fecha    | timestamp       | NO   |     | 0000-00-00 00:00:00 |                               |
| admin        | int(11)         | YES  |     | NULL      
*/

class Usuario{
    private $id;
    private $username;
    private $clave;
    private $rut;   
    private $nombre;
    private $apellidos;
    private $correo;
	private $edad;
	private $region;
    private $provincia;
    private $cod_articulo;
    private $comuna;    
    private $reg_fecha;
    private $mod_fecha;
    private $admin;


    function __construct($username, $clave)
    {       
        $this->id = 0;
        $this->username = $username;
        $this->clave = $clave;
        $this->rut = "";
        $this->nombre = "";
        $this->apellido = "";
        $this->correo = "";
        $this->edad = 0;
        $this->region = "";
        $this->provincia = "";
        $this->comuna = "";
        $this->reg_fecha = null;
        $this->mod_fecha = null;
        $this->admin = null;
    }

    public function getId(){
        return $this->id;
    }     
    public function getUsername(){
        return $this->username;
    }
    public function getClave(){
        return $this->clave;
    }
    public function getRut(){
        return $this->rut;
    }

    public function getNombre(){
        return $this->nombre;
    }

    public function getApellidos(){
        return $this->apellidos;
    }
    public function getCorreo(){
        return $this->correo;
    }
    public function getEdad(){
        return $this->edad;
    }
    public function getRegion(){
        return $this->region;
    }
    public function getProvincia(){
        return $this->provincia;
    }
    public function getComuna(){
        return $this->comuna;
    }
    public function getReg_fecha(){
        return $this->reg_fecha;
    }
    public function getMod_fecha(){
        return $this->mod_fecha;
    }
    public function getAdmin(){
        return $this->admin;
    }

    public function setUsername($username){
        $this->username = $username;
    } 
    public function setClave($clave){
        $this->clave = $clave;
    }
    public function setRut($rut){
        $this->rut = $rut;
    }

    public function setNombre($nombre){
        $this->nombre = $nombre;
    }

    public function setApellidos($apellidos){
        $this->apellidos = $apellidos;
    }
    public function setCorreo($correo){
        $this->correo = $correo;
    }
    public function setEdad($edad){
        $this->edad = $edad;
    }
    public function setRegion($region){
        $this->region = $region;
    }
    public function setProvincia($provincia){
        $this->provincia = $provincia;
    }
    public function setComuna($comuna){
        $this->comuna = $comuna;
    }
    public function setReg_fecha($reg_fecha){
        $this->reg_fecha = $reg_fecha;
    }
    public function setAdmin($admin){
        $this->admin = $admin;
    }

    function to_string(){

        return $this->ususername."-".$this->clave."-".$this->admin;
    }


}

?>