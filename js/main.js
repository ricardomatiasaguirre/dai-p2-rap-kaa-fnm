   // Las funcionalidades de la edad y autollenado de provincia y 
   // comuna deben ser con jquery 3 en un archivo js aparte llamado "main.js" 
   // en la carpeta /js
   $(function () {
    Regiones.forEach(function (value, index) {
        id_region = value.id_region;
        nombre = value.nombre
        option = "<option value='" + id_region + "'>" + nombre + "</option>"
        $("#region").append(option)
    })

    $("#region").on('change', function () {
        $("#provincia").html("<option>-- Seleccione --</option>");
        $("#comuna").html("<option>-- Seleccione --</option>");
        id_region = this.value
        var provincias = Provincias.filter(function (provincia) {
            return provincia.id_region == id_region
        })
        provincias.forEach(function (value) {
            id_provincia = value.id_provincia;
            nombre = value.nombre
            option = "<option value='" + id_provincia + "'>" + nombre + "</option>"
            $("#provincia").append(option)
        })
    })

    $("#provincia").on('change', function () {
        $("#comuna").html("<option>-- Seleccione --</option>");
        id_provincia = this.value
        var comunas = Comunas.filter(function (value) {
            return value.id_provincia == id_provincia
        })
        comunas.forEach(function(value){
            id_comuna = value.id_comuna;
            nombre = value.nombre
            option = "<option value='" + id_comuna + "'>" + nombre + "</option>"
            $("#comuna").append(option)
        })
    })

})


function isValidDate(day,month,year)
   {
     var dteDate;
   
     month=month-1;
     dteDate=new Date(year,month,day);
   
     return ((day==dteDate.getDate())&&(month==dteDate.getMonth())&&(year==dteDate.getFullYear()));
   
   }
   
   function validate_fecha(fecha)
   {
     var patron=new RegExp("^(19|20)+([0-9]{2})([-])([0-9]{1,2})([-])([0-9]{1,2})$");
   
     if(fecha.search(patron)==0)
     {
       var values=fecha.split("-");
       if(isValidDate(values[2],values[1],values[0]))
       {
         return true;
       }
     }
     return false;
   
   }
   
   function calcularEdad()
   {
     var fecha=document.getElementById("user_date").value;
     if(validate_fecha(fecha)==true)
     {
       var values=fecha.split("-");
       var dia = values[2];
       var mes = values[1];
       var ano = values[0];
   
       var fecha_hoy = new Date();
       var ahora_ano = fecha_hoy.getYear();
       var ahora_mes = fecha_hoy.getMonth()+1;
       var ahora_dia = fecha_hoy.getDate();
   
       var edad = (ahora_ano + 1900) - ano;
       if(ahora_mes < mes)
       {
         edad--;
       }
       if ((mes == ahora_mes) && (ahora_dia < dia))
       {
         edad--;
       }
       if (edad > 1900)
       {
         edad -= 1900;
       }
   
       var meses=0;
       if(ahora_mes>mes)
         meses=ahora_mes-mes;
       if(ahora_mes<mes)
         meses=12-(mes-ahora_mes);
       if(ahora_mes==mes && dia>ahora_dia)
         meses=11;
   
       var dias=0;
       if(ahora_dia>dia)
         dias=ahora_dia-dia;
       if(ahora_dia<dia)
       {
         ultimoDiaMes=new Date(ahora_ano, ahora_mes, 0);
         dias=ultimoDiaMes.getDate()-(dia-ahora_dia);
       }
   
       document.getElementById("edad").value="Tienes "+edad+" años, "+meses+" meses y "+dias+"dia ";
     }else{
       document.getElementById("edad").value="La fecha "+fecha+" es incorrecta";
       
     }
   }    
    