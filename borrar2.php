<?php
include_once './Entidades/Usuario.php';
include_once './Negocio/Clientes.php';

session_start();
if (!isset($_SESSION["user"])) {

    header("Location: ./login.php");
    exit();
} else {
    $usuario = $_SESSION["user"];
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">

    <TITLE>Borrar2.php</TITLE>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="./css/estilo.css">

</head>

<body>
    <nav class="navbar navbar-dark bg-dark">
        <div class="container">
            <label class="navbar-brand">Usuario: @<?php echo $usuario->getNombre();    ?></label>
            <span class="text-muted"><a href="https://gitlab.com/dai002/ejercicio-php-1/tree/mysql" target="_blank">https://gitlab.com/dai002/ejercicio-php-1/tree/mysql</a></span>
        </div>
    </nav>
    <div class="container">
        <?php
        $nombre = $_POST["nombre"];

        //Conexion con la base
        $borrado = false;
        $clientes = new Negocio\Clientes();
        $borrado = $clientes->eliminarCliente($nombre);
        ?>

        <h1>
            <div class="container">
                <?php if ($borrado) {
                    echo 'Registro Borrado';
                } else {
                    echo 'Registrno NO Borrado';
                }
                ?>


            </div>
        </h1>
        <br>
        <div><a href="lectura.php">Visualizar el contenido de la base</a></div>

        <footer class="footer mt-auto py-3">
            <div class="container">
                <span class="text-muted"><a href="https://gitlab.com/dai002/ejercicio-php-1/tree/mysql" target="_blank">https://gitlab.com/dai002/ejercicio-php-1/tree/mysql</a></span>
            </div>
        </footer>
    </div>
</body>

</html>