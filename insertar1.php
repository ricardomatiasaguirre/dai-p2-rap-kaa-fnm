<?php
include 'Entidades/Articulo.php';
include './Entidades/Usuario.php';

session_start();
if (!isset($_SESSION["user"])) {

    header("Location: ./login.php");
    exit();
} else {
    $usuario = $_SESSION["user"];
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <TITLE>Insertar.html</TITLE>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="./css/estilo.css">
</head>

<body>
    <nav class="navbar navbar-dark bg-dark">
        <div class="container">
            <label class="navbar-brand">Usuario: @<?php echo $usuario->getNombre();    ?></label>
            <span class="text-muted"><a href="https://gitlab.com/ricardomatiasaguirre/dai-p2-rap-kaa-fnm" target="_blank">Link Gitlab</a></span>
        </div>
    </nav>

    </head>


<div class="container">

 <div class="text-center">
   <h2>Creación Articulo</h2>
 </div>

 <div class="container"style="width:1000px">

  <form action="insertar2.php" method="POST">

    <div class="form-group">
       
      <label for="Titulo">Titulo:</label>
      <input required type="text" class="form-control" id="txtTitulo" placeholder="Ingrese Titulo del Articulo" name="txtTitulo" maxlength="80">

    </div>

    <div class="form-group">

      <label for="Contenido">Artículo:</label>
      <textarea rows="10" cols="100" id="txtArticulo" placeholder="Ingrese su Artículo..." name="txtArticulo"></textarea>
    </div>

    <div class="form-group">
                <input type="SUBMIT" class="btn btn-primary" value="Insertar">
                <a class="btn btn-secondary" href="lectura.php">Volver</a>

            </div>

   <div class="form-group">

        <form action="upload.php" method="post" enctype="multipart/form-data">
          Seleccione imagen para subir:
          <input type="file" name="imagen" id="imagenUpload">
          <input type="submit" value="Subir Imagen" name="submit">

      </form>

    </div>

    <div class="form-group">
        <p>Fecha Creación <span id="fechaCreacion" name="fechaC"></span></p>
        <script>
            var fechaCreacionArticulo = new Date();
            document.getElementById("fechaCreacion").innerHTML = fechaCreacionArticulo.toLocaleDateString();
            </script>
    </div>
    
    
  </form>
    
 </div>


 </div>

</div>

</body>


</html>