<?php

namespace Negocio;

ini_set("display_errors",1);
ini_set("display_startup_errors",1);

include_once 'bd.php';
include_once 'Entidades/Articulo.php';

use Entidades as ent;

class Articulo
{
    private $bd;
    function __construct()
    {
        $this->bd = new BD();
    }

    public function guardarArticulo($articulo)
    {
        //$fechaC = date(DD/MM/YY);
        $query = "INSERT INTO Articulo (nombre,descripcion) VALUES ($articulo->nombre,$articulo->descripcion)";
        return $this->bd->query($query);
    }

    public function obtenerListaArticulos()
    {
        $result = $this->bd->query("select nombre, descripcion, img, fecha_creacion ,fecha_mod from Articulo");
        $articulos = [];
        foreach ($result as $articulo) {
            array_push($articulos, new ent\articulo($articulo["nombre"], $articulo["descripcion"], $articulo["img"], $articulo["fecha_creacion"], $articulo["fecha_mod"]));
        }
        return $articulos;
    }

    public function eliminarArticulo($nombre)
    {
        $SQL = "Delete From Articulo Where nombre='$nombre'";

        return $this->bd->query($SQL);
    }

    public function agregarArticulo($articulo)
    {
        $nombre = $articulo->getNombre();
        $descripcion  = $articulo->getDescripcion();
        $SQL = "insert into Articulo (nombre,descripcion) values ('$nombre','$descripcion')";

        return $this->bd->query($SQL);
    }


    public function actualizarDescripcion($articulo)
    {

        $nombre = $articulo->getNombre();
        $descripcion = $articulo->getDescripcion();
        $SQL = "Update Articulo Set descripcion='$descripcion' Where nombre='$nombre'";
        return $this->bd->query($SQL);
    }
}







/*
<?php
include_once '../uploads/articuloDto.php';



    $art = new Articulo();
    $art->setTitulo(trim($_POST["titulo"]));
    $art->setContenido(trim($_POST["articulo"]));
    $art->setFechaCreacion(trim($_POST["fechaC"]));
    $art->setFechaModificacion(null);
    $img = $_POST["imagen"];


    $target_dir = "uploads/";
    $target_file = $target_dir . basename($_FILES["imagen"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["imagen"]["tmp_name"]);
    if($check !== false) {
        echo "Archivo es imagen valida - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "Lo siento, el archivo no es una imagen.";
        $uploadOk = 0;
    }
}

if (file_exists($target_file)) {
    echo "Lo siento, el archivo ya existe.";
    $uploadOk = 0;
}

if ($_FILES["imagen"]["size"] > 500000) {
    echo "Lo siento, el archivo es muy grande.";
    $uploadOk = 0;
}

if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Lo siento, solo archivos JPG, JPEG, PNG y GIF son permitidos.";
    $uploadOk = 0;
}

if ($uploadOk == 0) {
    echo "Lo siento, su imagen no se puso subir.";

} else {
    if (move_uploaded_file($_FILES["imagen"]["tmp_name"], $target_file)) {
        echo "El archivo ". basename( $_FILES["imagen"]["name"]). " ha sido subido con exito!.";
    } else {
        echo "Lo siento, hubo un error al subir su imagen.";
    }
}
?>

*/
