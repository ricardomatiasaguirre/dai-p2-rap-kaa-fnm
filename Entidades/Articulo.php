<?php

namespace Entidades;

class Articulo{

    private $nombre;
    private $descripcion;
    private $img;
    private $fechaCreacion;
    private $fechaModificacion;

    function __construct($nombre,$descripcion,$img,$fechaCreacion,$fechaModificacion){
        $this->nombre = $nombre;
        $this->descripcion=$descripcion;
        $this->img="";
        $this->fechaCreacion=$fechaCreacion;
        $this->fechaModificacion="";
        
    }

    public function getNombre(){
        return $this->nombre;
    }

    public function getDescripcion(){
        return $this->descripcion;
    }
    
    public function getImg(){
        return $this->img;
    }

    public function getFechaCreacion(){
        return $this->fechaCreacion;
    }

    public function getFechaModificacion(){
        return $this->fechaModificacion;
    }

    public function setNombre($nombre){
        $this->nombre=$nombre;
    }

    public function setDescripcion($descripcion){
        $this->descripcion=$descripcion;
    }

    public function setImg($img){
        $this->img=$img;
    }

    public function setFechaCreacion($fechaCreacion){
        $this->fechaCreacion=$fechaCreacion;
    }

    public function setFechaModificacion($fechaModificacion){
        $this->fechaModificacion=$fechaModificacion;
    }
}
/*
   
$art = new Articulo();
    $art->setTitulo(trim($_POST["titulo"]));
    $art->setContenido(trim($_POST["articulo"]));
    $art->setFechaCreacion(trim($_POST["fechaC"]));
    $art->setFechaModificacion(null);
    $img = $_POST["imagen"];


    $target_dir = "uploads/";
    $target_file = $target_dir . basename($_FILES["imagen"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["imagen"]["tmp_name"]);
    if($check !== false) {
        echo "Archivo es imagen valida - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "Lo siento, el archivo no es una imagen.";
        $uploadOk = 0;
    }
}

if (file_exists($target_file)) {
    echo "Lo siento, el archivo ya existe.";
    $uploadOk = 0;
}

if ($_FILES["imagen"]["size"] > 500000) {
    echo "Lo siento, el archivo es muy grande.";
    $uploadOk = 0;
}

if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Lo siento, solo archivos JPG, JPEG, PNG y GIF son permitidos.";
    $uploadOk = 0;
}

if ($uploadOk == 0) {
    echo "Lo siento, su imagen no se puso subir.";

} else {
    if (move_uploaded_file($_FILES["imagen"]["tmp_name"], $target_file)) {
        echo "El archivo ". basename( $_FILES["imagen"]["name"]). " ha sido subido con exito!.";
    } else {
        echo "Lo siento, hubo un error al subir su imagen.";
    }
}
*/
?>

