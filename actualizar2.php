<?php
include_once './Negocio/Usuario.php';
include_once './Negocio/Articulo.php';
include './Entidades/Usuario.php';
use Entidades as ent;
use Negocio as neg;

session_start();
if (!isset($_SESSION["user"])) {

    header("Location: ./login.php");
    exit();
} else {
    $usuario = $_SESSION["user"];
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <TITLE>Insertar.php</TITLE>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="./css/estilo.css">

</head>

<body>
    <nav class="navbar navbar-dark bg-dark">
        <div class="container">
            <label class="navbar-brand">Usuario: @<?php echo $usuario->getNombre();    ?></label>
            <span class="text-muted"><a href="https://gitlab.com/ricardomatiasaguirre/dai-p2-rap-kaa-fnm" target="_blank">Link Gitlab</a></span>
        </div>
    </nav>
    <div class="container">

        <?php
        $username = $_POST["user"];
        $rut = $_POST["rut"];
        $nombre = $_POST["nombre"];
        $apellidos = $_POST["apellidos"];
        $correo = $_POST["email"];
        $edad = $_POST["edad"];
        $region = $_POST["region"];
        $provincia = $_POST["provincia"];
        //$cod_articulo = $_POST["txtTitulo"];
        $comuna = $_POST["comuna"];
        //$fechaCreacion = "fechaC";
        $fecha_mod = "fechaC";
        //$admin = $_POST["txtTitulo"];
        $usuario = new ent\Usuario($username, $rut, $nombre, $apellidos, $correo, $edad, $region, $provincia, $comuna,  $fecha_mod);

        $usuarios = new neg\Usuario();
        $insertado = $usuarios->modificarUsuario($usuario);


        ?>

        <h1>
            <div class="container">
                <?php
                if ($insertado) {
                    echo 'Usuario Modificado con Exito';
                } else {
                    echo 'Usuario no Modificado';
                    

                }
                ?>
            </div>
        </h1>

        <br>
        <div><a href="lectura.php">Visualizar el contenido de la base</a></div>

        <footer class="footer mt-auto py-3">
            <div class="container">
                <span class="text-muted"><a href="https://gitlab.com/ricardomatiasaguirre/dai-p2-rap-kaa-fnm" target="_blank">Link Gitlab</a></span>
            </div>
        </footer>
    </div>
</body>

</html>