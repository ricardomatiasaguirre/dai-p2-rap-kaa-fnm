<?php
include_once 'Entidades/Usuario.php';
include_once './Negocio/Usuario.php';
use Entidades as ent;
use Negocio as neg;

$resultado;
if ($_SERVER['REQUEST_METHOD'] == "POST") {

    $user = $_POST["user"];
    $pass = $_POST["pass"];

    $usuario = new neg\Usuario();
    
    $resultado = $usuario->obtenerUsuario(new ent\Usuario($user, $pass));
}
if (isset($resultado)) {
    session_start();
    $_SESSION["user"] = $resultado;
    header("Location: ./lectura.php");
    exit();
} else {

    ?>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="UTF-8">
        <title>Login</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="./css/estilo.css">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
        </script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
        </script>
    </head>

    <body>
    <nav class="navbar navbar-dark bg-dark">
            <div class="container">
                <label class="navbar-brand">Usuario: @<?php echo $usuario->getNombre();    ?></label>
                <span class="text-muted"><a href="https://gitlab.com/ricardomatiasaguirre/dai-p2-rap-kaa-fnm" target="_blank">Link Gitlab</a></span>
            </div>
        </nav>
        <div class="container">

            <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">

                <main role="main" class="inner cover">
                    <h1 class="cover-heading">No tiene autorización.</h1>
                    <p class="lead">
                        <a href="./index.html" class="btn btn-lg btn-secondary">Salir</a>
                    </p>
                </main>



            </div>

            <footer class="footer mt-auto py-3">
                <div class="container">
                    <span class="text-muted"><a href="https://gitlab.com/ricardomatiasaguirre/dai-p2-rap-kaa-fnm" target="_blank">Link Gitlab</a></span>
                </div>
            </footer>
        </div>
    </body>

    </html>

<?php

}
?>