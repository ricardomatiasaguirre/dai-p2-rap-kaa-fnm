<?php
include_once './Negocio/Clientes.php';
include_once './Negocio/Clientes.php';
include './Entidades/Usuario.php';
use Negocio as neg;

session_start();
if (!isset($_SESSION["user"])) {

    header("Location: ./login.php");
    exit();
} else {
    $usuario = $_SESSION["user"];
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">

    <title>Borrar1.php</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="./css/estilo.css">

</head>

<body>
    <nav class="navbar navbar-dark bg-dark">
        <div class="container">
            <label class="navbar-brand">Usuario: @<?php echo $usuario->getNombre();    ?></label>
            <span class="text-muted"><a href="https://gitlab.com/dai002/ejercicio-php-1/tree/mysql" target="_blank">https://gitlab.com/dai002/ejercicio-php-1/tree/mysql</a></span>
        </div>
    </nav>
    <div class="container">

        <h1>Borrar un registro</h1>

        <br>
        <form METHOD="POST" ACTION="borrar2.php">Nombre<br>
            <div class="form-group">
                <select name="nombre" class="form-control">

                    <?php

                    $cliente = new neg\Clientes();
                    $clientes = $cliente->obtenerListaClientes();
                    foreach ($clientes as $cli) {
                        echo '<option>' . $cli->getNombre();
                    }

                    ?>

                </select>

            </div>
            <div class="form-group">
                <input TYPE="SUBMIT" class="btn btn-primary" value="Borrar">
                <a class="btn btn-secondary" href="lectura.php">Volver</a>

            </div>
        </form>

        <footer class="footer mt-auto py-3">
            <div class="container">
                <span class="text-muted"><a href="https://gitlab.com/dai002/ejercicio-php-1/tree/mysql" target="_blank">https://gitlab.com/dai002/ejercicio-php-1/tree/mysql</a></span>
            </div>
        </footer>
    </div>

</body>

</html>