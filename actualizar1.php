<?php
include './Entidades/Usuario.php';

session_start();
if (!isset($_SESSION["user"])) {

    header("Location: ./login.php");
    exit();
} else {
    $usuario = $_SESSION["user"];
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <TITLE>Insertar.html</TITLE>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="./css/estilo.css">

    <script  type="text/javascript" src="js/localidades.js"> </script>
    <script  type="text/javascript" src="js/main.js"> </script>
</head>

<body>
    <nav class="navbar navbar-dark bg-dark">
        <div class="container">
            <label class="navbar-brand">Usuario: @<?php echo $usuario->getNombre();    ?></label>
            <span class="text-muted"><a href="https://gitlab.com/ricardomatiasaguirre/dai-p2-rap-kaa-fnm" target="_blank">Link Gitlab</a></span>
        </div>
    </nav>

    </head>


<div class="container">

 <div class="text-center">
   <h2>Modificar datos personales</h2>
 </div>

 <div class="container"style="width:1000px">

  <form action="actualizar2.php" method="POST">
     
  <div class="form-group" >
                    <label for="user">Usuario:</label>
                    <input required type="text" class="form-control" id="user" placeholder="<?php echo $usuario->getUsername();    ?>" name="user" maxlength="12" readonly>
            </div>

    <div class="form-group" >
                    <label for="rut">Rut:</label>
                    <input required type="text" class="form-control" id="rut" placeholder="Ingrese Rut" name="rut" maxlength="12">
            </div>
			
            <div class="form-group">
                <label for="nombre">Nombre:</label>
                <input required type="text" pattern="[A-Za-z]+"placeholder="Ingrese Nombre"class="form-control" id="nombre" name="nombre" minlength="3" maxlength="100">
            </div>
            <div class="form-group">
                <label for="apellido">Apellidos:</label>
                <input required  type="text" class="form-control" id="apellidos" placeholder="Ingrese Apellido" name="apellidos" minlength="3" maxlength="13" >
            </div>
            <div class="form-group" >
                <label for="email">Email:</label>
                <input required type="email" class="form-control"  id="email" placeholder="Ingrese Email" name="email">
            </div>
          <div  class="form-group" id="derecha">
            <label for="edad">Edad:</label>
            <input  required type="text" class="form-control" id="edad" name="edad" >
          </div>
        
          <div class="form-group" >
                Región: 
                <br>
                <select class="custom-select" name="region" id="region">
                    <option selected value="0">-- Seleccione --</option>
                </select>
                <br>
                <br>
                Provincia:
                <br>
                <select class="custom-select" name="provincia" id="provincia">
                    <option selected value="0">-- Seleccione --</option>
                </select>
                <br>
                <br>
                Comuna: 
                <br>
                <select class="custom-select" name="comuna" id="comuna">
                    <option selected value="0">-- Seleccione --</option>
                </select>
          </div>

    <div class="form-group">
                <input type="SUBMIT" class="btn btn-primary" value="Insertar">
                <a class="btn btn-secondary" href="lectura.php">Volver</a>

            </div>

    <div class="form-group">
        <p>Fecha Creación <span id="fechaCreacion" name="fechaC"></span></p>
        <script>
            var fechaCreacionArticulo = new Date();
            document.getElementById("fechaCreacion").innerHTML = fechaCreacionArticulo.toLocaleDateString();
            </script>
    </div>
    
    
  </form>
    
 </div>


 </div>

</div>

</body>


</html>