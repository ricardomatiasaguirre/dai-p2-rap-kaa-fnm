<?php
include 'Entidades/Usuario.php';
session_start();

if (!isset($_SESSION["user"])) {

    header("Location: ./login.php");
    exit();
} else {
    $usuario = $_SESSION["user"];
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">

    <TITLE>lectura.php</TITLE>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="./css/estilo.css">

</head>

<body>
   <nav class="navbar navbar-dark bg-dark">
            <div class="container">
                <label class="navbar-brand">Usuario: @<?php echo $usuario->getNombre();    ?></label>
                <span class="text-muted"><a href="https://gitlab.com/ricardomatiasaguirre/dai-p2-rap-kaa-fnm" target="_blank">Link Gitlab</a></span>
            </div>
        </nav>
    </nav>
    <div class="container">

        <h1>
            <div>Articulos:</div>
        </h1>

        <br>

        <br>

        <?php

        //Conexion con la base
        include 'Negocio/Articulo.php';

        $articulo = new Negocio\Articulo();

        $articulos = $articulo->obtenerListaArticulos();

        ?>

        <table class="table">
            <thead>
                <tr>
                 
                    <th scope="col">Nombre</th>
                    <th scope="col">Descripción</th>
                    <th scope="col">Imagen</th>
                    <th scope="col">Fecha de Creación</th>
                    <th scope="col">Última modificación</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (sizeof($articulos) > 0) {

                    foreach ($articulos as $_articulo) {

                        echo '<tr><td>' . $_articulo->getNombre() . '</td>';

                        echo '<td>' . $_articulo->getDescripcion() . '</td>';

                        echo  '<td>' . $_articulo->getImg() . '</td>';

                        echo  '<td>' . $_articulo->getFechaCreacion() . '</td>';

                        echo  '<td>' . $_articulo->getFechaModificacion() . '</td>';
                        
                    }
                } else {
                    echo '<tr><td colspan="2">No hay articulos Disponibles</td></tr>';
                }
                ?>
            </tbody>
        </table>

        <div class="row">
            <div class="col">
                <a href="insertar1.php">Añadir un nuevo Articulo</a><br>
            </div>
            <div class="col">
                <a href="actualizar1.php">Actualizar información de su cuenta</a><br>
            </div>
            <div class="col">
                <a href="borrar1.php">Borrar un registro</a><br>
            </div>
        </div>

        <footer class="footer mt-auto py-3">
            <div class="container">
                <span class="text-muted"><a href="https://gitlab.com/ricardomatiasaguirre/dai-p2-rap-kaa-fnm" target="_blank">Link Gitlab</a></span>
            </div>
        </footer>
    </div>
</body>

</html>